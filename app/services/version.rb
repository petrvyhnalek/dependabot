# frozen_string_literal: true

class Version
  VERSION = "0.30.0"
end
