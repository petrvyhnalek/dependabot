# Release notification

::: warning
Not applicable for standalone mode
:::

Application exposes api endpoint that allows triggering update for particular dependency across all configured projects.
