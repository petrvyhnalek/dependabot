# UI

Simple UI is served at the index page of application, like `http://localhost:3000/`.

This page displays all projects and it's ecosystems currently handled by the application.

It allows to synchronize state with GitLab via `sync` button and also to manually trigger dependency update run via `execute` button.
