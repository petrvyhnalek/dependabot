# 🚀 Feature request

Suggest an idea for dependabot-gitlab

## Is there an existing issue for this?

Please search existing issues to avoid creating duplicates

- [ ] I have searched the existing issues

## Feature description

<!-- Describe the feature you'd like. -->
